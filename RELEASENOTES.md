# 0.1.0 First tagged Crossref bundle

2019-06-28

Current versions:

 - AccessIndicators.xsd
 - clinicaltrials.xsd
 - common4.4.2.xsd
 - crossref4.4.2.xsd
 - doi_resources4.4.2.xsd
 - fundref.xsd
 - grant_id.0.0.1.xsd
 - relations.xsd

New in this release:

 - common4.4.2.xsd
 - crossref4.4.2.xsd
 - doi_resources4.4.2.xsd
 - grant_id.0.0.1.xsd

Released deposit schema v. 4.4.2 and added first version of Grant IDs.
Added to 4.4.2:
- support for Pending Publication
- support for DUL
- support for JATS 1.2 abstracts
- add abstract support to dissertations, reports, and allow multiple abstracts wherever available
- add support for multiple dissertation authors
- add acceptance_date element to journal article, books, book chapter, conference paper